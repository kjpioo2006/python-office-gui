import sys

from PySide6.QtWidgets import QApplication
from customizeWindowPyfile.FinalWidget import FinalWidget
from qt_material import apply_stylesheet

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWidget = FinalWidget()
    mainWidget.show()
    apply_stylesheet(app, theme='dark_teal.xml')  # 设置Qt程序皮肤

    sys.exit(app.exec_())
