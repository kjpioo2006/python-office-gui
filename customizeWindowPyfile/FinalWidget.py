# coding:utf-8
from PySide6.QtCore import QDir, Slot
# 导入转换功能文件
from function.from2turn import from2turn
# 导入word功能文件
from function.Word.wordReplaceKeywords import wordReplaceKeyword
# 导入ppt功能文件
from function.Ppt.pptImageExtraction import pptImageExtration
# 导入文件功能文件
from function.File.cleanDuplicateFiles import cleanDuplicateFiles
from function.File.fileClassification import fileClasscation
# 导入PDF功能
from function.Pdf.decry_encryptionPDF import decry_encryptionPDF
from function.Pdf.createWatermark4PDF import createWatermark4PDF
# 鼠标拖动事件管理器,用于监听拖动文件事件，可通过鼠标拖动来获取文件路径
from EventHandler import QEventHandler
from customizeWindowPyfile.ui2pyFile.ui_widget import Ui_Widget
# 引入产生的子窗口
from PySide6.QtWidgets import QWidget, QFileDialog, QMessageBox
from customizeWindowPyfile.PasswordDialog import PasswordDialog
from customizeWindowPyfile.KeywordTableWindow import KeywordTableWindow
from customizeWindowPyfile.WatermarkDialog import WatermarkDialog
from PySide6 import QtGui


class FinalWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.ui = Ui_Widget()
        self.ui.setupUi(self)
        self.setWindowTitle('办公自动化工具')

        icon = QtGui.QIcon()
        icon.addPixmap(
            QtGui.QPixmap("../resource/picture/office365.png"), QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.setWindowIcon(icon)

        # 装上事件监听器--实现拖入文件时自动显示文件路径
        # 文件处理
        self.ui.pathFile2XML.installEventFilter(QEventHandler(self))
        self.ui.pathXML2File.installEventFilter(QEventHandler(self))
        self.ui.pathDuplicateFile.installEventFilter(QEventHandler(self))
        self.ui.pathFileClassication.installEventFilter(QEventHandler(self))
        # PPT处理
        self.ui.pathPPT2PDF.installEventFilter(QEventHandler(self))
        self.ui.pathPPT2JPG.installEventFilter(QEventHandler(self))
        self.ui.pathImageExtract.installEventFilter(QEventHandler(self))
        # Word处理
        self.ui.pathWord2PDF.installEventFilter(QEventHandler(self))
        self.ui.pathDoc2Docx.installEventFilter(QEventHandler(self))
        self.ui.pathDocx2Doc.installEventFilter(QEventHandler(self))
        self.ui.pathKeywordReplace.installEventFilter(QEventHandler(self))
        # PDF处理
        self.ui.pathPDF2Word.installEventFilter(QEventHandler(self))
        self.ui.pathDecrypt.installEventFilter(QEventHandler(self))
        self.ui.pathEncryption.installEventFilter(QEventHandler(self))
        self.ui.pathAddWatermark.installEventFilter(QEventHandler(self))

    # 定义选择路径的槽函数
    # 文件处理
    @Slot()
    def on_chooseButtonFile2XML_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathFile2XML.setText(aDir)

    @Slot()
    def on_singleChooseButtonFile2XML_clicked(self):
        curDir = QDir.currentPath()
        # 全部类型的文件都可选择
        aDir = QFileDialog.getOpenFileName(self, "选择一个文件", curDir)
        self.ui.pathFile2XML.setText(aDir[0])

    @Slot()
    def on_chooseButtonXML2File_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathXML2File.setText(aDir)

    @Slot()
    def on_singleChooseButtonXML2File_clicked(self):
        curDir = QDir.currentPath()
        # xml类型的文件才可选择
        aDir = QFileDialog.getOpenFileName(self, "选择一个文件", curDir, '*xml*')
        self.ui.pathXML2File.setText(aDir[0])

    @Slot()
    def on_chooseDuplicateFileButton_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathDuplicateFile.setText(aDir)

    @Slot()
    def on_chooseButtonFileClassification_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathFileClassication.setText(aDir)

    # PPT处理
    @Slot()
    def on_chooseButtonPPT2PDF_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathPPT2PDF.setText(aDir)

    @Slot()
    def on_singleChooseButtonPPT2PDF_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getOpenFileName(self, "选择一个文件", curDir, '*ppt*')
        self.ui.pathPPT2PDF.setText(aDir[0])

    @Slot()
    def on_chooseButtonPPT2JPG_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathPPT2JPG.setText(aDir)

    @Slot()
    def on_singleChooseButtonPPT2JPG_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getOpenFileName(self, "选择一个文件", curDir, '*ppt*')
        self.ui.pathPPT2JPG.setText(aDir[0])

    @Slot()
    def on_chooseButtonImageExtract_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getOpenFileName(self, "选择一个文件", curDir, '*ppt*')
        # QFileDialog.getOpenFileName()的返回值是一个元组，只需要元组的第一个值
        self.ui.pathImageExtract.setText(aDir[0])

    # Word处理
    @Slot()
    def on_chooseButtonWord2PDF_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathWord2PDF.setText(aDir)

    @Slot()
    def on_singleChooseButtonWord2PDF_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getOpenFileName(self, "选择一个文件", curDir, '*docx*')
        self.ui.pathWord2PDF.setText(aDir[0])

    @Slot()
    def on_chooseButtonDoc2Docx_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathDoc2Docx.setText(aDir)

    @Slot()
    def on_singleChooseButtonDoc2Docx_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getOpenFileName(self, "选择一个文件", curDir, '*doc*')
        self.ui.pathDoc2Docx.setText(aDir[0])

    @Slot()
    def on_chooseButtonDocx2Doc_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathDocx2Doc.setText(aDir)

    @Slot()
    def on_singleChooseButtonDocx2Doc_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getOpenFileName(self, "选择一个文件", curDir, '*docx*')
        self.ui.pathDocx2Doc.setText(aDir[0])

    @Slot()
    def on_chooseButtonKeywordReplace_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathKeywordReplace.setText(aDir)

    # PDF处理
    @Slot()
    def on_chooseButtonPDF2Word_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathPDF2Word.setText(aDir)

    @Slot()
    def on_singleChooseButtonPDF2Word_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getOpenFileName(self, "选择一个文件", curDir, '*pdf*')
        self.ui.pathPDF2Word.setText(aDir[0])

    @Slot()
    def on_choosePathAddWatermarkButton_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathAddWatermark.setText(aDir)

    @Slot()
    def on_singleChoosePathAddWatermarkButton_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getOpenFileName(self, "选择一个文件", curDir, '*pdf*')
        self.ui.pathAddWatermark.setText(aDir[0])

    @Slot()
    def on_choosePathEncryption_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathEncryption.setText(aDir)

    @Slot()
    def on_choosePathDecryption_clicked(self):
        curDir = QDir.currentPath()
        aDir = QFileDialog.getExistingDirectory(self, "选择一个目录", curDir, QFileDialog.Option.ShowDirsOnly)
        self.ui.pathDecrypt.setText(aDir)

    # 定义开始按钮的槽函数
    # 文件处理
    @Slot()
    def on_convertButtonFile2XML_clicked(self):
        from2turn(self.ui.pathFile2XML.text(), 'File', 'XML')
        QMessageBox.information(self, '提示框', '文件已经转换成XML！')

    @Slot()
    def on_convertButtonXML2File_clicked(self):
        from2turn(self.ui.pathXML2File.text(), 'XML', 'File')
        QMessageBox.information(self, '提示框', 'XML已经转换成对应格式文件！')

    @Slot()
    def on_duplicateFileButton_clicked(self):
        cleanDuplicateFiles(self.ui.pathDuplicateFile.text())
        QMessageBox.information(self, '提示框', '已经找出冗余文件！！')

    @Slot()
    def on_classificationButton_clicked(self):
        fileClasscation(self.ui.pathFileClassication.text())
        QMessageBox.information(self, '提示框', '文件已完成分类！')

    # PPT处理
    @Slot()
    def on_convertButtonPPT2PDF_clicked(self):
        from2turn(self.ui.pathPPT2PDF.text(), 'PPT', 'PDF')
        QMessageBox.information(self, '提示框', 'ppt已经转换成pdf！')

    @Slot()
    def on_convertButtonPPT2JPG_clicked(self):
        from2turn(self.ui.pathPPT2JPG.text(), 'PPT', 'JPG')
        QMessageBox.information(self, '提示框', 'ppt已经转换成JPG！')

    @Slot()
    def on_ExtractButton_clicked(self):
        pptImageExtration(self.ui.pathImageExtract.text())
        QMessageBox.information(self, '提示框', '选中ppt里面包含的图片已经被提取！！')

    # Word处理
    @Slot()
    def on_convertButtonWord2PDF_clicked(self):
        from2turn(self.ui.pathWord2PDF.text(), 'Word', 'PDF')
        QMessageBox.information(self, '提示框', 'word已经转换成pdf！')

    @Slot()
    def on_convertButtonDoc2Docx_clicked(self):
        from2turn(self.ui.pathDoc2Docx.text(), 'Doc', 'Docx')
        QMessageBox.information(self, '提示框', 'Doc已经转换成Docx！')

    @Slot()
    def on_convertButtonDocx2Doc_clicked(self):
        from2turn(self.ui.pathDocx2Doc.text(), 'Docx', 'Doc')
        QMessageBox.information(self, '提示框', 'Docx已经转换成Doc！')

    @Slot()  # 这里是打开关键字的窗口
    def on_adjustButtonKeyword_clicked(self):
        keywordeywordTableWindow = KeywordTableWindow(self)
        keywordeywordTableWindow.confirmSignal.connect(self.getKeywordDictAndReplace)
        keywordeywordTableWindow.show()

    @Slot(dict)
    def getKeywordDictAndReplace(self, dict):
        dir = dict
        wordReplaceKeyword(self.ui.pathKeywordReplace.text(), dir)
        QMessageBox.information(self, '提示框', '文件中的关键字已经替换完毕！')

    # PDF处理
    @Slot()
    def on_convertButtonPDF2Word_clicked(self):
        print('选择路径' + self.ui.pathPDF2Word.text())
        from2turn(self.ui.pathPDF2Word.text(), 'PDF', 'Word')
        QMessageBox.information(self, '提示框', 'pdf已经转换成word！')

    # 添加自定义水印操作的窗口
    @Slot()
    def on_addWatermarkButton_clicked(self):
        watermarkDialog = WatermarkDialog(self)
        watermarkDialog.confirmSignal.connect(self.getWatermarkWordAndAddWatermark)
        watermarkDialog.show()

    @Slot(list)
    def getWatermarkWordAndAddWatermark(self, list):
        watermarkWord = list[0]
        angkle = list[1]
        alpha = list[2]
        color = list[3]
        path = self.ui.pathAddWatermark.text()
        createWatermark4PDF(watermarkWord, path, color, angkle, alpha)
        QMessageBox.information(self, '提示框', '水印添加成功！！')

    # 以下是有产生子窗口的情况
    @Slot()  # 加密窗口--输入加密密码
    def on_encryptionButton_clicked(self):
        passwordDialog = PasswordDialog(self)
        passwordDialog.passwordSignal.connect(self.setPassWordandEncryption)
        passwordDialog.show()  # 用exec()方法可以阻塞窗口
        # passwordDialog.exec_()

    @Slot()  # 解密窗口--输入解密密码
    def on_DecryptionButton_clicked(self):
        passwordDialog = PasswordDialog(self)
        passwordDialog.passwordSignal.connect(self.setPasswordAndDecryption)
        passwordDialog.show()  # 用exec()方法可以阻塞窗口

    @Slot(str)
    def setPassWordandEncryption(self, str):
        password = str
        decry_encryptionPDF(self.ui.pathEncryption.text(), password, 'encry')
        QMessageBox.information(self, '提示框', 'PDF加密成功！')

    @Slot(str)
    def setPasswordAndDecryption(self, str):
        password = str
        decry_encryptionPDF(self.ui.pathDecrypt.text(), password, 'decry')
        QMessageBox.information(self, '提示框', 'PDF解密成功！')
