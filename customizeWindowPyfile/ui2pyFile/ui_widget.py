# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'widget.ui'
##
## Created by: Qt User Interface Compiler version 6.5.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QHBoxLayout, QLabel,
    QLineEdit, QPushButton, QSizePolicy, QSpacerItem,
    QTabWidget, QVBoxLayout, QWidget)

class Ui_Widget(object):
    def setupUi(self, Widget):
        if not Widget.objectName():
            Widget.setObjectName(u"Widget")
        Widget.resize(832, 624)
        icon = QIcon()
        icon.addFile(u"../resource/picture/office365.png", QSize(), QIcon.Normal, QIcon.Off)
        Widget.setWindowIcon(icon)
        self.verticalLayout_6 = QVBoxLayout(Widget)
        self.verticalLayout_6.setSpacing(6)
        self.verticalLayout_6.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(-1, -1, -1, 0)
        self.verticalLayout_5 = QVBoxLayout()
        self.verticalLayout_5.setSpacing(6)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setSpacing(6)
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.horizontalLayout_8.setContentsMargins(100, -1, 130, -1)
        self.label_9 = QLabel(Widget)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setPixmap(QPixmap(u"../../resource/picture/office365.png"))
        self.label_9.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_8.addWidget(self.label_9)

        self.label_8 = QLabel(Widget)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setMaximumSize(QSize(16777215, 16777215))
        font = QFont()
        font.setFamilies([u"Arial Black"])
        font.setPointSize(25)
        font.setBold(True)
        self.label_8.setFont(font)
        self.label_8.setTextFormat(Qt.PlainText)
        self.label_8.setPixmap(QPixmap(u"../../resource/picture/office365.png"))
        self.label_8.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.label_8)


        self.verticalLayout_5.addLayout(self.horizontalLayout_8)

        self.tabWidget = QTabWidget(Widget)
        self.tabWidget.setObjectName(u"tabWidget")
        font1 = QFont()
        font1.setFamilies([u"Arial"])
        font1.setPointSize(11)
        font1.setBold(True)
        self.tabWidget.setFont(font1)
        self.tabWidget.setTabPosition(QTabWidget.North)
        self.tabWidget.setTabShape(QTabWidget.Rounded)
        self.tabWidget.setIconSize(QSize(40, 40))
        self.tabWidget.setElideMode(Qt.ElideLeft)
        self.tabWidget.setUsesScrollButtons(True)
        self.tabWidget.setDocumentMode(False)
        self.tabWidget.setTabsClosable(False)
        self.tabWidget.setMovable(False)
        self.tabWidget.setTabBarAutoHide(False)
        self.tab_3 = QWidget()
        self.tab_3.setObjectName(u"tab_3")
        self.verticalLayout_14 = QVBoxLayout(self.tab_3)
        self.verticalLayout_14.setSpacing(6)
        self.verticalLayout_14.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.frame_4 = QFrame(self.tab_3)
        self.frame_4.setObjectName(u"frame_4")
        self.frame_4.setFrameShape(QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QFrame.Raised)
        self.verticalLayout_13 = QVBoxLayout(self.frame_4)
        self.verticalLayout_13.setSpacing(6)
        self.verticalLayout_13.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.verticalLayout_7 = QVBoxLayout()
        self.verticalLayout_7.setSpacing(6)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setSpacing(6)
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.labelFile2XML = QLabel(self.frame_4)
        self.labelFile2XML.setObjectName(u"labelFile2XML")

        self.horizontalLayout_10.addWidget(self.labelFile2XML)

        self.pathFile2XML = QLineEdit(self.frame_4)
        self.pathFile2XML.setObjectName(u"pathFile2XML")

        self.horizontalLayout_10.addWidget(self.pathFile2XML)

        self.chooseButtonFile2XML = QPushButton(self.frame_4)
        self.chooseButtonFile2XML.setObjectName(u"chooseButtonFile2XML")
        icon1 = QIcon()
        icon1.addFile(u"../resource/picture/path.png", QSize(), QIcon.Normal, QIcon.Off)
        self.chooseButtonFile2XML.setIcon(icon1)

        self.horizontalLayout_10.addWidget(self.chooseButtonFile2XML)

        self.singleChooseButtonFile2XML = QPushButton(self.frame_4)
        self.singleChooseButtonFile2XML.setObjectName(u"singleChooseButtonFile2XML")
        self.singleChooseButtonFile2XML.setIcon(icon1)

        self.horizontalLayout_10.addWidget(self.singleChooseButtonFile2XML)

        self.convertButtonFile2XML = QPushButton(self.frame_4)
        self.convertButtonFile2XML.setObjectName(u"convertButtonFile2XML")
        icon2 = QIcon()
        icon2.addFile(u"../resource/picture/start.png", QSize(), QIcon.Normal, QIcon.Off)
        self.convertButtonFile2XML.setIcon(icon2)

        self.horizontalLayout_10.addWidget(self.convertButtonFile2XML)


        self.verticalLayout_7.addLayout(self.horizontalLayout_10)

        self.horizontalLayout_16 = QHBoxLayout()
        self.horizontalLayout_16.setSpacing(6)
        self.horizontalLayout_16.setObjectName(u"horizontalLayout_16")
        self.label_14 = QLabel(self.frame_4)
        self.label_14.setObjectName(u"label_14")

        self.horizontalLayout_16.addWidget(self.label_14)

        self.pathXML2File = QLineEdit(self.frame_4)
        self.pathXML2File.setObjectName(u"pathXML2File")

        self.horizontalLayout_16.addWidget(self.pathXML2File)

        self.chooseButtonXML2File = QPushButton(self.frame_4)
        self.chooseButtonXML2File.setObjectName(u"chooseButtonXML2File")
        self.chooseButtonXML2File.setIcon(icon1)

        self.horizontalLayout_16.addWidget(self.chooseButtonXML2File)

        self.singleChooseButtonXML2File = QPushButton(self.frame_4)
        self.singleChooseButtonXML2File.setObjectName(u"singleChooseButtonXML2File")
        self.singleChooseButtonXML2File.setIcon(icon1)

        self.horizontalLayout_16.addWidget(self.singleChooseButtonXML2File)

        self.convertButtonXML2File = QPushButton(self.frame_4)
        self.convertButtonXML2File.setObjectName(u"convertButtonXML2File")
        self.convertButtonXML2File.setIcon(icon2)

        self.horizontalLayout_16.addWidget(self.convertButtonXML2File)


        self.verticalLayout_7.addLayout(self.horizontalLayout_16)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setSpacing(6)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.label_5 = QLabel(self.frame_4)
        self.label_5.setObjectName(u"label_5")

        self.horizontalLayout_6.addWidget(self.label_5)

        self.pathDuplicateFile = QLineEdit(self.frame_4)
        self.pathDuplicateFile.setObjectName(u"pathDuplicateFile")

        self.horizontalLayout_6.addWidget(self.pathDuplicateFile)

        self.chooseDuplicateFileButton = QPushButton(self.frame_4)
        self.chooseDuplicateFileButton.setObjectName(u"chooseDuplicateFileButton")
        self.chooseDuplicateFileButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.chooseDuplicateFileButton.setIcon(icon1)

        self.horizontalLayout_6.addWidget(self.chooseDuplicateFileButton)

        self.duplicateFileButton = QPushButton(self.frame_4)
        self.duplicateFileButton.setObjectName(u"duplicateFileButton")
        self.duplicateFileButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.duplicateFileButton.setIcon(icon2)

        self.horizontalLayout_6.addWidget(self.duplicateFileButton)


        self.verticalLayout_7.addLayout(self.horizontalLayout_6)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label = QLabel(self.frame_4)
        self.label.setObjectName(u"label")

        self.horizontalLayout.addWidget(self.label)

        self.pathFileClassication = QLineEdit(self.frame_4)
        self.pathFileClassication.setObjectName(u"pathFileClassication")

        self.horizontalLayout.addWidget(self.pathFileClassication)

        self.chooseButtonFileClassification = QPushButton(self.frame_4)
        self.chooseButtonFileClassification.setObjectName(u"chooseButtonFileClassification")
        self.chooseButtonFileClassification.setCursor(QCursor(Qt.PointingHandCursor))
        self.chooseButtonFileClassification.setIcon(icon1)

        self.horizontalLayout.addWidget(self.chooseButtonFileClassification)

        self.classificationButton = QPushButton(self.frame_4)
        self.classificationButton.setObjectName(u"classificationButton")
        self.classificationButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.classificationButton.setIcon(icon2)

        self.horizontalLayout.addWidget(self.classificationButton)


        self.verticalLayout_7.addLayout(self.horizontalLayout)


        self.verticalLayout_13.addLayout(self.verticalLayout_7)


        self.verticalLayout_14.addWidget(self.frame_4)

        icon3 = QIcon()
        icon3.addFile(u"../resource/picture/file1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.tabWidget.addTab(self.tab_3, icon3, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_2 = QVBoxLayout(self.tab_2)
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout_2.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.frame_2 = QFrame(self.tab_2)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.frame_2)
        self.verticalLayout_4.setSpacing(6)
        self.verticalLayout_4.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setSpacing(6)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_2 = QLabel(self.frame_2)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout_2.addWidget(self.label_2)

        self.pathPPT2PDF = QLineEdit(self.frame_2)
        self.pathPPT2PDF.setObjectName(u"pathPPT2PDF")
        self.pathPPT2PDF.setCursor(QCursor(Qt.IBeamCursor))
        self.pathPPT2PDF.setDragEnabled(True)

        self.horizontalLayout_2.addWidget(self.pathPPT2PDF)

        self.chooseButtonPPT2PDF = QPushButton(self.frame_2)
        self.chooseButtonPPT2PDF.setObjectName(u"chooseButtonPPT2PDF")
        self.chooseButtonPPT2PDF.setCursor(QCursor(Qt.PointingHandCursor))
        self.chooseButtonPPT2PDF.setIcon(icon1)

        self.horizontalLayout_2.addWidget(self.chooseButtonPPT2PDF)

        self.singleChooseButtonPPT2PDF = QPushButton(self.frame_2)
        self.singleChooseButtonPPT2PDF.setObjectName(u"singleChooseButtonPPT2PDF")
        self.singleChooseButtonPPT2PDF.setIcon(icon1)

        self.horizontalLayout_2.addWidget(self.singleChooseButtonPPT2PDF)

        self.convertButtonPPT2PDF = QPushButton(self.frame_2)
        self.convertButtonPPT2PDF.setObjectName(u"convertButtonPPT2PDF")
        self.convertButtonPPT2PDF.setCursor(QCursor(Qt.PointingHandCursor))
        self.convertButtonPPT2PDF.setIcon(icon2)

        self.horizontalLayout_2.addWidget(self.convertButtonPPT2PDF)


        self.verticalLayout_3.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setSpacing(6)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.label_3 = QLabel(self.frame_2)
        self.label_3.setObjectName(u"label_3")

        self.horizontalLayout_3.addWidget(self.label_3)

        self.pathPPT2JPG = QLineEdit(self.frame_2)
        self.pathPPT2JPG.setObjectName(u"pathPPT2JPG")
        self.pathPPT2JPG.setCursor(QCursor(Qt.IBeamCursor))
        self.pathPPT2JPG.setDragEnabled(True)

        self.horizontalLayout_3.addWidget(self.pathPPT2JPG)

        self.chooseButtonPPT2JPG = QPushButton(self.frame_2)
        self.chooseButtonPPT2JPG.setObjectName(u"chooseButtonPPT2JPG")
        self.chooseButtonPPT2JPG.setCursor(QCursor(Qt.PointingHandCursor))
        self.chooseButtonPPT2JPG.setIcon(icon1)

        self.horizontalLayout_3.addWidget(self.chooseButtonPPT2JPG)

        self.singleChooseButtonPPT2JPG = QPushButton(self.frame_2)
        self.singleChooseButtonPPT2JPG.setObjectName(u"singleChooseButtonPPT2JPG")
        self.singleChooseButtonPPT2JPG.setIcon(icon1)

        self.horizontalLayout_3.addWidget(self.singleChooseButtonPPT2JPG)

        self.convertButtonPPT2JPG = QPushButton(self.frame_2)
        self.convertButtonPPT2JPG.setObjectName(u"convertButtonPPT2JPG")
        self.convertButtonPPT2JPG.setCursor(QCursor(Qt.PointingHandCursor))
        self.convertButtonPPT2JPG.setIcon(icon2)

        self.horizontalLayout_3.addWidget(self.convertButtonPPT2JPG)


        self.verticalLayout_3.addLayout(self.horizontalLayout_3)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setSpacing(6)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_6 = QLabel(self.frame_2)
        self.label_6.setObjectName(u"label_6")

        self.horizontalLayout_7.addWidget(self.label_6)

        self.pathImageExtract = QLineEdit(self.frame_2)
        self.pathImageExtract.setObjectName(u"pathImageExtract")

        self.horizontalLayout_7.addWidget(self.pathImageExtract)

        self.chooseButtonImageExtract = QPushButton(self.frame_2)
        self.chooseButtonImageExtract.setObjectName(u"chooseButtonImageExtract")
        self.chooseButtonImageExtract.setCursor(QCursor(Qt.PointingHandCursor))
        self.chooseButtonImageExtract.setIcon(icon1)

        self.horizontalLayout_7.addWidget(self.chooseButtonImageExtract)

        self.ExtractButton = QPushButton(self.frame_2)
        self.ExtractButton.setObjectName(u"ExtractButton")
        self.ExtractButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.ExtractButton.setIcon(icon2)

        self.horizontalLayout_7.addWidget(self.ExtractButton)


        self.verticalLayout_3.addLayout(self.horizontalLayout_7)


        self.verticalLayout_4.addLayout(self.verticalLayout_3)


        self.verticalLayout_2.addWidget(self.frame_2)

        icon4 = QIcon()
        icon4.addFile(u"../resource/picture/powerpoint1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.tabWidget.addTab(self.tab_2, icon4, "")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.verticalLayout = QVBoxLayout(self.tab)
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.frame = QFrame(self.tab)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_12 = QVBoxLayout(self.frame)
        self.verticalLayout_12.setSpacing(6)
        self.verticalLayout_12.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.verticalLayout_11 = QVBoxLayout()
        self.verticalLayout_11.setSpacing(6)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setSpacing(6)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.labelWord2Pdf = QLabel(self.frame)
        self.labelWord2Pdf.setObjectName(u"labelWord2Pdf")

        self.horizontalLayout_4.addWidget(self.labelWord2Pdf)

        self.pathWord2PDF = QLineEdit(self.frame)
        self.pathWord2PDF.setObjectName(u"pathWord2PDF")
        self.pathWord2PDF.setCursor(QCursor(Qt.IBeamCursor))
        self.pathWord2PDF.setAcceptDrops(True)
        self.pathWord2PDF.setDragEnabled(True)

        self.horizontalLayout_4.addWidget(self.pathWord2PDF)

        self.chooseButtonWord2PDF = QPushButton(self.frame)
        self.chooseButtonWord2PDF.setObjectName(u"chooseButtonWord2PDF")
        self.chooseButtonWord2PDF.setCursor(QCursor(Qt.PointingHandCursor))
        self.chooseButtonWord2PDF.setIcon(icon1)

        self.horizontalLayout_4.addWidget(self.chooseButtonWord2PDF)

        self.singleChooseButtonWord2PDF = QPushButton(self.frame)
        self.singleChooseButtonWord2PDF.setObjectName(u"singleChooseButtonWord2PDF")
        self.singleChooseButtonWord2PDF.setIcon(icon1)

        self.horizontalLayout_4.addWidget(self.singleChooseButtonWord2PDF)

        self.convertButtonWord2PDF = QPushButton(self.frame)
        self.convertButtonWord2PDF.setObjectName(u"convertButtonWord2PDF")
        self.convertButtonWord2PDF.setCursor(QCursor(Qt.PointingHandCursor))
        self.convertButtonWord2PDF.setIcon(icon2)

        self.horizontalLayout_4.addWidget(self.convertButtonWord2PDF)


        self.verticalLayout_11.addLayout(self.horizontalLayout_4)

        self.horizontalLayout_11 = QHBoxLayout()
        self.horizontalLayout_11.setSpacing(6)
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.label_10 = QLabel(self.frame)
        self.label_10.setObjectName(u"label_10")

        self.horizontalLayout_11.addWidget(self.label_10)

        self.pathDoc2Docx = QLineEdit(self.frame)
        self.pathDoc2Docx.setObjectName(u"pathDoc2Docx")

        self.horizontalLayout_11.addWidget(self.pathDoc2Docx)

        self.chooseButtonDoc2Docx = QPushButton(self.frame)
        self.chooseButtonDoc2Docx.setObjectName(u"chooseButtonDoc2Docx")
        self.chooseButtonDoc2Docx.setIcon(icon1)

        self.horizontalLayout_11.addWidget(self.chooseButtonDoc2Docx)

        self.singleChooseButtonDoc2Docx = QPushButton(self.frame)
        self.singleChooseButtonDoc2Docx.setObjectName(u"singleChooseButtonDoc2Docx")
        self.singleChooseButtonDoc2Docx.setIcon(icon1)

        self.horizontalLayout_11.addWidget(self.singleChooseButtonDoc2Docx)

        self.convertButtonDoc2Docx = QPushButton(self.frame)
        self.convertButtonDoc2Docx.setObjectName(u"convertButtonDoc2Docx")
        self.convertButtonDoc2Docx.setIcon(icon2)

        self.horizontalLayout_11.addWidget(self.convertButtonDoc2Docx)


        self.verticalLayout_11.addLayout(self.horizontalLayout_11)

        self.horizontalLayout_12 = QHBoxLayout()
        self.horizontalLayout_12.setSpacing(6)
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.label_11 = QLabel(self.frame)
        self.label_11.setObjectName(u"label_11")

        self.horizontalLayout_12.addWidget(self.label_11)

        self.pathDocx2Doc = QLineEdit(self.frame)
        self.pathDocx2Doc.setObjectName(u"pathDocx2Doc")

        self.horizontalLayout_12.addWidget(self.pathDocx2Doc)

        self.chooseButtonDocx2Doc = QPushButton(self.frame)
        self.chooseButtonDocx2Doc.setObjectName(u"chooseButtonDocx2Doc")
        self.chooseButtonDocx2Doc.setIcon(icon1)

        self.horizontalLayout_12.addWidget(self.chooseButtonDocx2Doc)

        self.singleChooseButtonDocx2Doc = QPushButton(self.frame)
        self.singleChooseButtonDocx2Doc.setObjectName(u"singleChooseButtonDocx2Doc")
        self.singleChooseButtonDocx2Doc.setIcon(icon1)

        self.horizontalLayout_12.addWidget(self.singleChooseButtonDocx2Doc)

        self.convertButtonDocx2Doc = QPushButton(self.frame)
        self.convertButtonDocx2Doc.setObjectName(u"convertButtonDocx2Doc")
        self.convertButtonDocx2Doc.setIcon(icon2)

        self.horizontalLayout_12.addWidget(self.convertButtonDocx2Doc)


        self.verticalLayout_11.addLayout(self.horizontalLayout_12)

        self.horizontalLayout_15 = QHBoxLayout()
        self.horizontalLayout_15.setSpacing(6)
        self.horizontalLayout_15.setObjectName(u"horizontalLayout_15")
        self.label_13 = QLabel(self.frame)
        self.label_13.setObjectName(u"label_13")

        self.horizontalLayout_15.addWidget(self.label_13)

        self.pathKeywordReplace = QLineEdit(self.frame)
        self.pathKeywordReplace.setObjectName(u"pathKeywordReplace")

        self.horizontalLayout_15.addWidget(self.pathKeywordReplace)

        self.chooseButtonKeywordReplace = QPushButton(self.frame)
        self.chooseButtonKeywordReplace.setObjectName(u"chooseButtonKeywordReplace")
        self.chooseButtonKeywordReplace.setIcon(icon1)

        self.horizontalLayout_15.addWidget(self.chooseButtonKeywordReplace)

        self.singleChooseButtonKeywordReplace = QPushButton(self.frame)
        self.singleChooseButtonKeywordReplace.setObjectName(u"singleChooseButtonKeywordReplace")
        self.singleChooseButtonKeywordReplace.setIcon(icon1)

        self.horizontalLayout_15.addWidget(self.singleChooseButtonKeywordReplace)

        self.adjustButtonKeyword = QPushButton(self.frame)
        self.adjustButtonKeyword.setObjectName(u"adjustButtonKeyword")
        icon5 = QIcon()
        icon5.addFile(u"../resource/picture/setting.png", QSize(), QIcon.Normal, QIcon.Off)
        self.adjustButtonKeyword.setIcon(icon5)

        self.horizontalLayout_15.addWidget(self.adjustButtonKeyword)


        self.verticalLayout_11.addLayout(self.horizontalLayout_15)


        self.verticalLayout_12.addLayout(self.verticalLayout_11)


        self.verticalLayout.addWidget(self.frame)

        icon6 = QIcon()
        icon6.addFile(u"../resource/picture/word1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.tabWidget.addTab(self.tab, icon6, "")
        self.tab_4 = QWidget()
        self.tab_4.setObjectName(u"tab_4")
        self.verticalLayout_10 = QVBoxLayout(self.tab_4)
        self.verticalLayout_10.setSpacing(6)
        self.verticalLayout_10.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.frame_3 = QFrame(self.tab_4)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setEnabled(True)
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.verticalLayout_8 = QVBoxLayout(self.frame_3)
        self.verticalLayout_8.setSpacing(6)
        self.verticalLayout_8.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_9 = QVBoxLayout()
        self.verticalLayout_9.setSpacing(6)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setSpacing(6)
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.labelPDF2Word = QLabel(self.frame_3)
        self.labelPDF2Word.setObjectName(u"labelPDF2Word")

        self.horizontalLayout_9.addWidget(self.labelPDF2Word)

        self.pathPDF2Word = QLineEdit(self.frame_3)
        self.pathPDF2Word.setObjectName(u"pathPDF2Word")

        self.horizontalLayout_9.addWidget(self.pathPDF2Word)

        self.chooseButtonPDF2Word = QPushButton(self.frame_3)
        self.chooseButtonPDF2Word.setObjectName(u"chooseButtonPDF2Word")
        self.chooseButtonPDF2Word.setIcon(icon1)

        self.horizontalLayout_9.addWidget(self.chooseButtonPDF2Word)

        self.singleChooseButtonPDF2Word = QPushButton(self.frame_3)
        self.singleChooseButtonPDF2Word.setObjectName(u"singleChooseButtonPDF2Word")
        self.singleChooseButtonPDF2Word.setIcon(icon1)

        self.horizontalLayout_9.addWidget(self.singleChooseButtonPDF2Word)

        self.convertButtonPDF2Word = QPushButton(self.frame_3)
        self.convertButtonPDF2Word.setObjectName(u"convertButtonPDF2Word")
        self.convertButtonPDF2Word.setIcon(icon2)

        self.horizontalLayout_9.addWidget(self.convertButtonPDF2Word)


        self.verticalLayout_9.addLayout(self.horizontalLayout_9)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setSpacing(6)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label_4 = QLabel(self.frame_3)
        self.label_4.setObjectName(u"label_4")

        self.horizontalLayout_5.addWidget(self.label_4)

        self.pathAddWatermark = QLineEdit(self.frame_3)
        self.pathAddWatermark.setObjectName(u"pathAddWatermark")

        self.horizontalLayout_5.addWidget(self.pathAddWatermark)

        self.choosePathAddWatermarkButton = QPushButton(self.frame_3)
        self.choosePathAddWatermarkButton.setObjectName(u"choosePathAddWatermarkButton")
        self.choosePathAddWatermarkButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.choosePathAddWatermarkButton.setIcon(icon1)

        self.horizontalLayout_5.addWidget(self.choosePathAddWatermarkButton)

        self.singleChoosePathAddWatermarkButton = QPushButton(self.frame_3)
        self.singleChoosePathAddWatermarkButton.setObjectName(u"singleChoosePathAddWatermarkButton")
        self.singleChoosePathAddWatermarkButton.setIcon(icon1)

        self.horizontalLayout_5.addWidget(self.singleChoosePathAddWatermarkButton)

        self.addWatermarkButton = QPushButton(self.frame_3)
        self.addWatermarkButton.setObjectName(u"addWatermarkButton")
        self.addWatermarkButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.addWatermarkButton.setIcon(icon5)

        self.horizontalLayout_5.addWidget(self.addWatermarkButton)


        self.verticalLayout_9.addLayout(self.horizontalLayout_5)

        self.horizontalLayout_13 = QHBoxLayout()
        self.horizontalLayout_13.setSpacing(6)
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.label_7 = QLabel(self.frame_3)
        self.label_7.setObjectName(u"label_7")

        self.horizontalLayout_13.addWidget(self.label_7)

        self.pathEncryption = QLineEdit(self.frame_3)
        self.pathEncryption.setObjectName(u"pathEncryption")

        self.horizontalLayout_13.addWidget(self.pathEncryption)

        self.choosePathEncryption = QPushButton(self.frame_3)
        self.choosePathEncryption.setObjectName(u"choosePathEncryption")
        self.choosePathEncryption.setCursor(QCursor(Qt.PointingHandCursor))
        self.choosePathEncryption.setIcon(icon1)

        self.horizontalLayout_13.addWidget(self.choosePathEncryption)

        self.encryptionButton = QPushButton(self.frame_3)
        self.encryptionButton.setObjectName(u"encryptionButton")
        self.encryptionButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.encryptionButton.setIcon(icon2)

        self.horizontalLayout_13.addWidget(self.encryptionButton)


        self.verticalLayout_9.addLayout(self.horizontalLayout_13)

        self.horizontalLayout_14 = QHBoxLayout()
        self.horizontalLayout_14.setSpacing(6)
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.label_12 = QLabel(self.frame_3)
        self.label_12.setObjectName(u"label_12")

        self.horizontalLayout_14.addWidget(self.label_12)

        self.pathDecrypt = QLineEdit(self.frame_3)
        self.pathDecrypt.setObjectName(u"pathDecrypt")

        self.horizontalLayout_14.addWidget(self.pathDecrypt)

        self.choosePathDecryption = QPushButton(self.frame_3)
        self.choosePathDecryption.setObjectName(u"choosePathDecryption")
        self.choosePathDecryption.setCursor(QCursor(Qt.PointingHandCursor))
        self.choosePathDecryption.setIcon(icon1)

        self.horizontalLayout_14.addWidget(self.choosePathDecryption)

        self.DecryptionButton = QPushButton(self.frame_3)
        self.DecryptionButton.setObjectName(u"DecryptionButton")
        self.DecryptionButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.DecryptionButton.setIcon(icon2)

        self.horizontalLayout_14.addWidget(self.DecryptionButton)


        self.verticalLayout_9.addLayout(self.horizontalLayout_14)


        self.verticalLayout_8.addLayout(self.verticalLayout_9)


        self.verticalLayout_10.addWidget(self.frame_3)

        icon7 = QIcon()
        icon7.addFile(u"../resource/picture/pdf1.png", QSize(), QIcon.Normal, QIcon.Off)
        self.tabWidget.addTab(self.tab_4, icon7, "")

        self.verticalLayout_5.addWidget(self.tabWidget)

        self.horizontalSpacer = QSpacerItem(531, 17, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout_5.addItem(self.horizontalSpacer)


        self.verticalLayout_6.addLayout(self.verticalLayout_5)


        self.retranslateUi(Widget)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Widget)
    # setupUi

    def retranslateUi(self, Widget):
        Widget.setWindowTitle(QCoreApplication.translate("Widget", u"Widget", None))
        self.label_9.setText("")
        self.label_8.setText(QCoreApplication.translate("Widget", u"\u529e\u516c\u81ea\u52a8\u5316\u5de5\u5177", None))
        self.labelFile2XML.setText(QCoreApplication.translate("Widget", u"\u6587\u4ef6\u8f6cXML\uff1a", None))
        self.chooseButtonFile2XML.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6\u5939", None))
        self.singleChooseButtonFile2XML.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.convertButtonFile2XML.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb\u8f6c\u6362", None))
        self.label_14.setText(QCoreApplication.translate("Widget", u"XML\u8f6c\u6587\u4ef6\uff1a", None))
        self.chooseButtonXML2File.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6\u5939", None))
        self.singleChooseButtonXML2File.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.convertButtonXML2File.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb\u8f6c\u6362", None))
        self.label_5.setText(QCoreApplication.translate("Widget", u"\u5197\u4f59\u6587\u4ef6\u5904\u7406\uff1a", None))
        self.pathDuplicateFile.setText("")
        self.chooseDuplicateFileButton.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6\u5939", None))
        self.duplicateFileButton.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb\u5904\u7406", None))
        self.label.setText(QCoreApplication.translate("Widget", u"\u6587\u4ef6\u5206\u7c7b\uff1a", None))
        self.pathFileClassication.setText("")
        self.chooseButtonFileClassification.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6\u5939", None))
        self.classificationButton.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb\u5904\u7406", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), QCoreApplication.translate("Widget", u" \u6587\u4ef6\u5904\u7406", None))
        self.label_2.setText(QCoreApplication.translate("Widget", u"PPT\u8f6cPDF:", None))
        self.pathPPT2PDF.setText("")
        self.chooseButtonPPT2PDF.setText(QCoreApplication.translate("Widget", u" \u9009\u62e9\u6587\u4ef6\u5939", None))
        self.singleChooseButtonPPT2PDF.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.convertButtonPPT2PDF.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb\u8f6c\u6362", None))
        self.label_3.setText(QCoreApplication.translate("Widget", u"PPT\u8f6cJPG:", None))
        self.pathPPT2JPG.setText("")
        self.chooseButtonPPT2JPG.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6\u5939", None))
        self.singleChooseButtonPPT2JPG.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.convertButtonPPT2JPG.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb\u8f6c\u6362", None))
        self.label_6.setText(QCoreApplication.translate("Widget", u"\u63d0\u53d6\u56fe\u7247:", None))
        self.chooseButtonImageExtract.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.ExtractButton.setText(QCoreApplication.translate("Widget", u" \u5f00\u59cb\u63d0\u53d6", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Widget", u"PPT\u5904\u7406", None))
        self.labelWord2Pdf.setText(QCoreApplication.translate("Widget", u"Word\uff08docx\uff09\u8f6cPDF\uff1a", None))
        self.pathWord2PDF.setText("")
        self.chooseButtonWord2PDF.setText(QCoreApplication.translate("Widget", u" \u9009\u62e9\u6587\u4ef6\u5939", None))
        self.singleChooseButtonWord2PDF.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.convertButtonWord2PDF.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb\u8f6c\u6362", None))
        self.label_10.setText(QCoreApplication.translate("Widget", u"Word\u683c\u5f0f-Doc\u8f6cDocx\uff1a", None))
        self.chooseButtonDoc2Docx.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6\u5939", None))
        self.singleChooseButtonDoc2Docx.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.convertButtonDoc2Docx.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb\u8f6c\u6362", None))
        self.label_11.setText(QCoreApplication.translate("Widget", u"Word\u683c\u5f0f-Docx\u8f6cDoc\uff1a", None))
        self.chooseButtonDocx2Doc.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6\u5939", None))
        self.singleChooseButtonDocx2Doc.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.convertButtonDocx2Doc.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb\u8f6c\u6362", None))
        self.label_13.setText(QCoreApplication.translate("Widget", u"\u5173\u952e\u5b57\u66ff\u6362:", None))
        self.pathKeywordReplace.setText("")
        self.chooseButtonKeywordReplace.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6\u5939", None))
        self.singleChooseButtonKeywordReplace.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.adjustButtonKeyword.setText(QCoreApplication.translate("Widget", u" \u8c03\u6574\u53c2\u6570", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Widget", u"Word\u5904\u7406", None))
        self.labelPDF2Word.setText(QCoreApplication.translate("Widget", u"PDF\u8f6cWord\uff08docx\uff09\uff1a", None))
        self.chooseButtonPDF2Word.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6\u5939", None))
        self.singleChooseButtonPDF2Word.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.convertButtonPDF2Word.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb\u8f6c\u6362", None))
        self.label_4.setText(QCoreApplication.translate("Widget", u"\u6dfb\u52a0\u6c34\u5370:", None))
        self.pathAddWatermark.setText("")
        self.choosePathAddWatermarkButton.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6\u5939", None))
        self.singleChoosePathAddWatermarkButton.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u6587\u4ef6", None))
        self.addWatermarkButton.setText(QCoreApplication.translate("Widget", u" \u53c2\u6570\u8bbe\u7f6e", None))
        self.label_7.setText(QCoreApplication.translate("Widget", u"\u6279\u91cf\u52a0\u5bc6:", None))
        self.pathEncryption.setText("")
        self.choosePathEncryption.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u8def\u5f84", None))
        self.encryptionButton.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb", None))
        self.label_12.setText(QCoreApplication.translate("Widget", u"\u6279\u91cf\u89e3\u5bc6:", None))
        self.pathDecrypt.setText("")
        self.choosePathDecryption.setText(QCoreApplication.translate("Widget", u"\u9009\u62e9\u8def\u5f84", None))
        self.DecryptionButton.setText(QCoreApplication.translate("Widget", u"\u5f00\u59cb", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), QCoreApplication.translate("Widget", u"PDF \u5904\u7406", None))
    # retranslateUi

