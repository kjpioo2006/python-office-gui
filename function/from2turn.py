# coding:utf-8
import office

from pathlib import Path
from customizeWindowPyfile.ProgressBarDialog import ProgressBar


def from2turn(FROMpath, from_type: str, to_type: str):
    """
    转换文件类型函数
    :param FROMpath: 文件或文件夹的所在路径
    :param from_type: 源类型
    :param to_type:目标类型
    :return:
    """
    des_folder = None
    file_list = []
    try:
        # 单个文件处理
        end_fix = FROMpath.split('.')[1]
        file_list = [Path(FROMpath)]
        from_file = Path(FROMpath)
        start = str(from_file.resolve()).strip(from_file.suffix)
        if from_type == 'PPT':
            if to_type == 'PDF':
                des_folder = Path(start + 'PPT转PDF')
            if to_type == 'JPG':
                des_folder = Path(start + 'PPT转JPG')
        if from_type == 'Word' and to_type == 'PDF':
            des_folder = Path(start + 'Word(docx)转PDF')
        if from_type == 'PDF' and to_type == 'Word':
            des_folder = Path(start + 'PDF转Word')
        if from_type == 'Docx' and to_type == 'Doc':
            des_folder = Path(start + 'Docx转Doc')
        if from_type == 'Doc' and to_type == 'Docx':
            des_folder = Path(start + 'Doc转Docx')
        if from_type == 'File' and to_type == 'XML':
            des_folder = Path(start + 'File转XML')
        if from_type == 'XML' and to_type == 'File':
            des_folder = Path(start + 'XML转File')
    except IndexError:
        # 多个文件处理
        src_folder = Path(FROMpath)
        if from_type == 'PPT':
            file_list = list(src_folder.glob('*.ppt*'))
            if to_type == 'PDF':
                des_folder = Path(FROMpath) / 'PPT转PDF'
            if to_type == 'JPG':
                des_folder = Path(FROMpath) / 'PPT转JPG'
        if from_type == 'Word' and to_type == 'PDF':
            file_list = list(src_folder.glob('*.docx*'))
            des_folder = Path(FROMpath) / 'Word(docx)转PDF'
        if from_type == 'PDF' and to_type == 'Word':
            file_list = list(src_folder.glob('*.pdf*'))
            des_folder = Path(FROMpath) / 'PDF转Word'
        if from_type == 'Docx' and to_type == 'Doc':
            file_list = list(src_folder.glob('*.docx*'))
            des_folder = src_folder / 'Docx转Doc'
        if from_type == 'Doc' and to_type == 'Docx':
            file_list = list(src_folder.glob('*.doc*'))
            des_folder = src_folder / 'Doc转Docx'
        if from_type == 'File' and to_type == 'XML':
            file_list = list(src_folder.glob('*'))
            des_folder = src_folder / 'File转XML'
        if from_type == 'XML' and to_type == 'File':
            file_list = list(src_folder.glob('*.xml*'))
            des_folder = src_folder / 'XML转File'

    # 创建文件夹
    if des_folder is not None and not des_folder.exists():
        des_folder.mkdir(parents=True)

    # 转换进度条
    totalTaskNum = len(file_list)
    doneTaskNum = 0
    currTaskNum = 1
    bar = ProgressBar()
    bar.setProcessOnTiTle(currTaskNum, totalTaskNum)
    bar.show()

    for file in file_list:
        # 转换完成的进度条显示
        if from_type == 'PDF' and to_type == 'Word':
            office.pdf.pdf2docx(file, des_folder)
        if from_type == 'PPT' and to_type == 'PDF':
            office.ppt.ppt2pdf(path=str(file), output_path=des_folder)
        if from_type == 'PPT' and to_type == 'JPG':
            office.ppt.ppt2img(input_path=file, output_path=des_folder)
        if from_type == 'Word' and to_type == 'PDF':
            office.word.docx2pdf(file, des_folder)
        if from_type == 'Doc' and to_type == 'Docx':
            office.word.doc2docx(file, des_folder)
        if from_type == 'Docx' and to_type == 'Doc':
            office.word.docx2doc(file, des_folder)
        if from_type in ['File', 'XML'] and to_type in ['File', 'XML']:
            # 设置文件的放置位置
            if from_type == 'File':
                des_name = f"{file.stem}-{file.suffix.split('.')[1]}.xml"
            else:
                des_name = f"{file.stem.split('-')[0]}.{file.stem.split('-')[1]}"
            des_file = des_folder / des_name
            # 将源文件内容写入新文件里--读取源文件写入新文件
            with open(des_file, 'wb') as f:
                with open(file, 'rb') as fp:
                    content = fp.read()
                    f.write(content)

        doneTaskNum = doneTaskNum + 1
        currTaskNum = currTaskNum + 1
        bar.setValue(100 * doneTaskNum / totalTaskNum, 100 * (doneTaskNum - 1) / totalTaskNum,
                     100 * totalTaskNum / totalTaskNum)


if __name__ == "__main__":
    from2turn('C:\Desktop\功能测试\ppt文件', 'PPT', 'PDF')
    from2turn('C:\Desktop\功能测试\ppt文件', 'PPT', 'JPG')
