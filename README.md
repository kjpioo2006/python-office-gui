# 参考说明
参考地址：

GitHub地址：https://github.com/CoderWanFeng/python-office

码云地址：https://gitee.com/CoderWanFeng/python-office


# 项目结构
该项目主要用办公自动化文件相关的处理

项目文件夹结构如下：
```shell
├─customizeWindowPyfile # UI界面文件转换后的py文件及绑定监听事件、信号与槽
│  ├─ui2pyFile      # UI文件转换为的py文件
├─customizeWindowUIfile # Qt UI界面文件
├─function # 自定义槽函数
│  ├─File # 文件处理槽函数
│  ├─Pdf # PDF处理槽函数
│  │  ├─font # 水印字体文件
│  ├─Ppt # PPT处理槽函数
│  ├─Word # Word处理槽函数
├─resource # icon文件
│  └─picture
```

重要文件说明：

![img.png](resource/UI-img/img-project.png)


# 项目技术点
项目技术点：

1.pyside6 --Qt程序设计

2.python-office --第三方office封装package


# 环境搭建
安装python3.8.10

安装依赖包：
```shell
$ pip install -r requirements.txt
```


# 项目运行及打包为exe
项目运行命令如下：
```shell
$ python main.py
```

在对应项目的虚拟环境下，执行项目打包为exe命令，具体如下：
```shell
# 命令打包
# 打包为单文件，启动慢
# --name:exe的名称
# --copy-metadata：手动添加模块，主要用于添加间接依赖包
# --icon：图标文件所在路径
# -F 将所有资源打包成只有一个exe的独立文件,main.py为Qt程序的入口文件
$ pyinstaller --name "office-tool-single" --copy-metadata pikepdf --icon "E:/pyinstaller-out/python-office-gui/resource/picture/icon.ico" -F main.py
# 打包为多文件，启动快
# -D 将所有资源打包成多文件的exe,main.py为Qt程序的入口文件
$ pyinstaller --name "office-tool-file" --copy-metadata pikepdf --icon "E:/pyinstaller-out/python-office-gui/resource/picture/icon.ico" -D main.py

# 配置文件打包
# xxx.spec：打包成功exe后所生成的配置文件，后续执行该命令可打包成新exe
# 打包为单文件
$ pyinstaller office-tool-single.spec
# 打包为多文件
$ pyinstaller office-tool-file.spec
```

# 项目UI界面
项目UI界面如下：
![img.png](resource/UI-img/img.png)
![img_1.png](resource/UI-img/img_1.png)
![img_2.png](resource/UI-img/img_2.png)
![img_3.png](resource/UI-img/img_3.png)
